﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> AddOrUpdateAsync(T data)
        {
            var isExited = Data.Any(x => x.Id == data.Id);

            if (!isExited)
            {
                 Data = Data.Append(data).ToList();
            } else
            {
                IEnumerable<T> tempData = new List<T>();
                foreach (var item in Data)
                {
                   tempData = data.Id == item.Id ?
                              tempData.Append(data) : tempData.Append(item);
                }
                Data = tempData;
            }

            return Task.FromResult(data);
        }

        public Task<IEnumerable<T>> DeleteAsync(Guid id)
        {
            var isExited = Data.Any(x => x.Id == id);

            if (isExited)
                Data = Data.Where(x => x.Id != id).ToList();

            return Task.FromResult(Data);
        }
    }
}