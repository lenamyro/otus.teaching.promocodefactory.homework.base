﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests
{
    public class EmployeeRequest
    {
    
        [Required]
        public string Id {
            get;
            set;
        }

        [Required]
        public string Email { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public List<string> Roles { get; set; }
    }
}
