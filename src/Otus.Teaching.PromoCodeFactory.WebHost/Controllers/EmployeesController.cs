﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Employees
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Get all Employees
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Get employee by  Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Add a new employee
        /// </summary>
        /// <remarks>
        ///     Sample <strong>request</strong>:
        ///         
        ///         POST /api/v1/employees
        ///         {
        ///             "email": "example@gmail.com",
        ///             "firstName": "George",
        ///             "lastName": "Rassel",
        ///             "roles": ["Admin", "PartnerManager"]
        ///         }
        /// </remarks>
        /// <param name="request">Required parameter EmployeeRequest</param>
        /// <response code="201">Employee was created succesfully</response>
        /// <response code="400">Bad request</response>
        /// <response code="409">Employee already exists</response>
        [HttpPost]
        public async Task<ActionResult> CreateEmployeeAsync(EmployeeRequest request)
        {
            if (request == null)
                return BadRequest();

            var oldEmployee = await _employeeRepository.GetByIdAsync(Guid.Parse(request.Id));
            if (oldEmployee != null)
                return StatusCode(409, $"User with Id = {request.Id} already exists ");

            var employee = new Employee()
            {
                Id = Guid.Parse(request.Id),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };

            var result = await _employeeRepository.AddOrUpdateAsync(employee);

            if (result.Id != null)
                return StatusCode(201);

            return StatusCode(500);

        }

        /// <summary>
        /// Update existed Employee
        /// </summary>
        /// <response code="200">Employee was updated succesfully</response>
        /// <response code="400">Bad request</response>
        /// <response code="404">Employee was not found</response>
        [HttpPut]
        public async Task<ActionResult> UpdateEmployeeAsync(EmployeeRequest request)
        {
            if (request == null)
                return BadRequest("Request is null");

            var employee = await _employeeRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (employee == null)
                return NotFound();

                var employee1 = new Employee()
                {
                    Id = Guid.Parse(request.Id),
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Email = request.Email
                };
                await _employeeRepository.AddOrUpdateAsync(employee1);

                return Ok();
        }

        /// <summary>
        /// Delete existed employee by Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<List<EmployeeShortResponse>> DeleteEmployeeAsync([Required] Guid id)
        {
            if (id == null)
                throw new Exception("Id is null");

            var employees = await _employeeRepository.DeleteAsync(id);

            var employeesModelList = employees.Select(x =>
              new EmployeeShortResponse()
              {
                  Id = x.Id,
                  Email = x.Email,
                  FullName = x.FullName,
              }).ToList();

            return employeesModelList;
        }
    }
}